import { Component, OnInit } from '@angular/core';
import { Projection } from 'src/app/models/projection/projection';
import { ProjectionsService } from 'src/app/providers/projections/projections.service';
import { LoadingController, NavParams, ModalController, ActionSheetController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Storage } from '@ionic/storage';
import { User } from 'src/app/models/user/user';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-projection-detail',
  templateUrl: './projection-detail.page.html',
  styleUrls: ['./projection-detail.page.scss'],
})



export class ProjectionDetailPage implements OnInit {

  isLoading = false;
  //commentPlaceHolder = "";
  public comment;
  public base64Images = null;
  user: User;
  comments = [];
  projection: Projection = {
    "id": "",
    "name": "",
    "deleted": false,
    "description": "",
    "createdAt": "",
    "modifiedAt": "",
    "subtitulo": "",
    "takeProfit1": "",
    "takeProfit2": "",
    "takeProfit3": "",
    "takeProfit4": "",
    "tipo": "",
    "entryPrice": "",
    "stopLoss": "",
    "buyLimit": "",
    "sellLimit": "",
    "createdById": "",
    "createdByName": "",
    "modifiedById": "",
    "modifiedByName": "",
    "assignedUserId": "",
    "assignedUserName": "",
    "imagenId": "",
    "imagenName": "",
    "urlimg": ""
  };
  id: string;

  constructor( 
    private pservice: ProjectionsService,
    public loadingController: LoadingController,
    private translate: TranslateService,
    public navParams: NavParams,
    private photoViewer: PhotoViewer,
    public modalCtrl: ModalController,
    public storage: Storage,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public _DomSanitizer: DomSanitizer
  ) {
    this.user = {
      id: '0',
      name: 'No user',
      emailAddress: 'No email',
      profilePicURL: 'nopic.jpg',
      idFireBase: '0'
    }
    this.id = navParams.get('id');
    //this.commentPlaceHolder = this.translate.instant('PROJECTION.commentPlaceHolder');
    //console.log(this.commentPlaceHolder);
    
    storage.get('user').then((val) => {
      this.user = val;
    });
   }

  async presentLoading(msg) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: msg
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss();
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss();
  }

  async doRefresh(event) {
    this.getProjection();
    this.getComments();
    event.target.complete();
  }

  ngOnInit() {
    this.getProjection();
    this.getComments();
  }

  getProjection(){
    this.presentLoading(this.translate.instant('LOADING.projection')); 
    this.pservice.getProjection(this.id).then((response) => {      
      this.projection = JSON.parse(response.data);
      this.projection.urlimg = 'http://kumovicci.com/jbcapital/data/upload/'+this.projection.imagenId;
      this.projection.createdAt = this.projection.createdAt+' UTC';
      this.dismiss();
      console.log(this.projection);
      
    }).catch((error: any) => {
      this.dismiss();
      alert(JSON.stringify(error));
    }); 
  }

  getComments(){
    //this.presentLoading(this.translate.instant('LOADING.commentsProjection')); 
    this.pservice.getProjectionComments(this.id).then((response) => {      
      let data = JSON.parse(response.data);
      Object.keys(data['list']).forEach(function(key,index) {
          let url = data['list'][key].imagenId != null ? 'http://kumovicci.com/jbcapital/data/upload/'+data['list'][key].imagenId : null;
          data['list'][key].urlimg = url;
          data['list'][key].createdAt = data['list'][key].createdAt+' UTC';
      });
      this.comments = data['list'];
      //this.dismiss();
    }).catch((error: any) => {
      this.dismiss();
      alert(JSON.stringify(error));
    }); 
  }

  showImage(img){
    this.photoViewer.show(img);
  }

  closeModal(){
    this.modalCtrl.dismiss();
  }

  async selectImgSource(){
    const actionSheet = await this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Tomar foto',
          role: 'destructive',
          handler: () => {
            this.takePhoto(1);
          }
        },{
          text: 'Galeria',
          handler: () => {
            this.takePhoto(0);
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    await actionSheet.present();    
  }

  takePhoto(sourceType:number) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType:sourceType,
    }

    this.camera.getPicture(options).then((imageData) => {
      let img_full = "data:image/jpg;base64," + imageData;
      this.base64Images = img_full;
    }, (err) => {
      // Handle error
    });
  }

  postComment(){

    this.presentLoading(this.translate.instant('LOADING.publishingComment')); 

    let fullcomment = {
      comment: this.comment,
      projectionId: this.projection.id,
      projectionName: this.projection.name,
      accountId: this.user.id,
      accountName: this.user.name,
      name: this.user.name,
      imagenId: null,
      imagenName: null
    }

    this.pservice.uploadImage(this.base64Images).then(response => {
      let data = JSON.parse(response.data);
      if(data.code != 2){
        fullcomment.imagenName = data.imageId;
        fullcomment.imagenId = data.imageId;
        this.pservice.postComment(fullcomment).then(r => {
          this.comment = null;
          this.base64Images = null;
          this.dismiss();
          this.refreshContent();
        }).catch(error => {
          console.log(error);
          this.dismiss();
        });
      }else{
        //error
      }
    }).catch(error => {
      console.log(error);
      this.dismiss();
    });    
  }

  refreshContent(){
    this.getProjection();
    this.getComments();
  }

}
