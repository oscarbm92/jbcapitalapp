import { Component, OnInit } from '@angular/core';
//import { Zoom } from '@ionic-native/zoom/ngx';

@Component({
  selector: 'app-conferences',
  templateUrl: './conferences.page.html',
  styleUrls: ['./conferences.page.scss'],
})
export class ConferencesPage implements OnInit {

  // meeting options (Only available for Android)
  private options = {
  "no_driving_mode":true,
  "no_invite":true,
  "no_meeting_end_message":true,
  "no_titlebar":false,
  "no_bottom_toolbar":false,
  "no_dial_in_via_phone":true,
  "no_dial_out_to_phone":true,
  "no_disconnect_audio":true,
  "no_share":true,
  "no_audio":true,
  "no_video":true,
  "no_meeting_error_message":true
  };

  constructor(
    //private zoomService: Zoom
  ) { }

  ngOnInit() {
    /*this.zoomService.initialize('wKAtC5gUS1uSpdc-4M4rfQ', 'kwQ86nOfYqIc44Jkn8g7n9gfwF0a3UrAuBdb').then((success: any) => {

    }).catch((error: any) => {

    });*/
  }

  async doRefresh(event) {
    
    event.target.complete();
  }

  joinMeeting(){
    // Join meeting.
    /*this.zoomService.joinMeeting('343-290-093', '628566', 'Oscar Bustos', this.options)
    .then((success: any) => console.log(success))
    .catch((error: any) => console.log(error));*/
  }

}
