import { Component, OnInit } from '@angular/core';
import { LanguageService } from './../../services/language.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  constructor(private translate: TranslateService, private languageService: LanguageService) { }

  languages = [];
  selected = '';

  ngOnInit() {
    this.languages = this.languageService.getLanguages();
    this.selected = this.languageService.selected;
    //console.log('languageService.selected->' +this.selected);
  }

  select(lng) {
    this.languageService.setLanguage(lng);
  }

}
