import { Component, OnInit } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { LoadingController, NavController, Events, AlertController, MenuController } from '@ionic/angular';
import { UserService } from '../../providers/user/user.service';
import { Storage } from '@ionic/storage';
import { User } from 'src/app/models/user/user';
import { TranslateService } from '@ngx-translate/core';
import { FCM } from "@ionic-native/fcm/ngx";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private user : User; 

  isLoading = false;

  constructor(
    private fb: Facebook,
    public loadingController: LoadingController,
    private fireAuth: AngularFireAuth,
    private navCtrl: NavController,
    private userService: UserService,
    private storage: Storage,
    public events: Events,
    private translate: TranslateService,
    public fcm: FCM,
    public alertCtrl: AlertController,
    public menuCtrl: MenuController
    ) { 

      this.menuCtrl.enable(false);

    }

  async presentLoading(msg) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: msg
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss();
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss();
  }

  ngOnInit() {
     
  }

  ionViewDidEnter(){
    this.checkUserStatus();   
  }

  checkUserStatus(){
    this.presentLoading(this.translate.instant('LOADING.checkSession')); 
    this.fireAuth.auth.onAuthStateChanged(userfb => {
      if (userfb) {
        this.loginCrmWithUid(userfb);
      }else {
        this.dismiss();
      }
    })
  }

  loginCrmWithUid(user){
    let t = this;
    this.userService.getUserWithUid(user.uid).then((response) => {      
      let userdata = JSON.parse(response.data);
      console.log(userdata);
      
      if(userdata['list'][0].licenciaActiva){
        t.fcm.subscribeToTopic('proyecciones');
        console.log("suscribiendose a: proyecciones");
      }else{
        t.fcm.unsubscribeFromTopic('proyecciones');
        console.log("desuscribiendose de: proyecciones");
      }

      t.fcm.getToken().then(token => {
        t.userService.setUserFirebaseToken(userdata['list'][0].id, token).then((responsetk) => { 

        },
        (error) =>{
          console.log(error);
        });
      }) // save the token server-side and use it to push notifications to this device
      .catch(error => console.error('Error getting token', error));

      t.fcm.onTokenRefresh().subscribe((token: string) => {
        t.userService.setUserFirebaseToken(userdata['list'][0].id, token).then((responsetk) => { 

        },
        (error) =>{
          console.log(error);
          if(error.error == 'The host could not be resolved'){
            alert('¡Necesitas estas conectado a internet!');
          }else if(error.status == 404 || error.status == 1){
            this.loginCrmWithUid(user);
          }
          
        });
      });

      this.events.publish('user:created', userdata['list'][0]);
      this.storage.set('user', userdata['list'][0]);
      this.dismiss();
      this.navCtrl.navigateRoot('/app/tabs/newsfeed');
    },
    (error) =>{
      console.log(error);
      if(error.error == 'The host could not be resolved'){
        this.showNoConexion();
      }
    })
  }

  loginWithFB(){
    this.presentLoading(this.translate.instant('LOADING.logging'));
    //Esconde el boton de login
    /*
      this.facebook.login(['email', 'public_profile', 'user_birthday']).then((response: FacebookLoginResponse) => {
      this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large),birthday', []).then(profile => {
        this.userData = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name'], birthday: profile['birthday']}
    */

    //Login con FB
    this.fb.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {      
      this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
        //this.username = profile['name'];
        //Login a firebase con FBLogin
        let credential = firebase.auth.FacebookAuthProvider.credential(response.authResponse.accessToken);
        firebase.auth().signInAndRetrieveDataWithCredential (credential).then((info) =>{ 
          if(info.additionalUserInfo.isNewUser){
            this.createCrmUser(info.user);
          }else{
            this.loginCrmWithUid(info.user);
          }
        });
      }).catch((error: any) => {
        alert(JSON.stringify(error));
      })
    }).catch((error: any) => {
        alert(JSON.stringify(error));
    })

  }

  createCrmUser(user){
    //Registrar usuario al CRM
    this.userService.newUser(user).then((response) => { 
      let data = JSON.parse(response.data);
      this.events.publish('user:created', data);
      this.storage.set('user', data);
      this.dismiss();
      this.navCtrl.navigateRoot('/app/tabs/newsfeed');
    },
    (error) =>{
      console.log(error);
    })
  }

  async showNoConexion(){
    const alert = await this.alertCtrl.create({
      header: '¡UPS!',
      subHeader: 'Parece que no estas conectado/a a internet, para usar esta aplicación es necesario una conexión a una red WiFi o datos móviles.',
      buttons: [
        {
          text: 'Intentar de nuevo',
          handler: () => {
            this.checkUserStatus();
          }
        },
        {
          text: 'Salir',
          handler: () => {
            navigator['app'].exitApp();
          }
        }
      ]
    });
    await alert.present();
  }

}
