import { Component, OnInit } from '@angular/core';
import { NewsfeedService } from 'src/app/providers/newsfeed/newsfeed.service';
import { LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-newsfeed',
  templateUrl: './newsfeed.page.html',
  styleUrls: ['./newsfeed.page.scss'],
})
export class NewsfeedPage implements OnInit {

  isLoading = false;
  newsfeed = [];

  constructor(
    public loadingController: LoadingController,
    public nfservice: NewsfeedService,
    private translate: TranslateService,
    private iab: InAppBrowser
  ) { }

  async presentLoading(msg) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: msg
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss();
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss();
  }

  ngOnInit() {
    this.getNewsFeed();
  }

  async doRefresh(event) {
    await this.getNewsFeed();
    event.target.complete();
  }

  getNewsFeed(){
    this.presentLoading(this.translate.instant('LOADING.newsfeed')); 
    this.nfservice.getNewsFeed().then((response) => {      
      let data = JSON.parse(response.data);
      let p = data['list'];
      Object.keys(data['list']).forEach(function(key,index) {
          let url = 'http://kumovicci.com/jbcapital/data/upload/'+data['list'][key].imagenId;
          data['list'][key].urlimg = url;
          data['list'][key].createdAt = data['list'][key].createdAt+' UTC';
      });
      this.newsfeed = p;
      console.log(this.newsfeed);
      
      this.dismiss();
    }).catch((error: any) => {
      this.dismiss();
      alert(JSON.stringify(error));
    });
  }

  openPost(url){
    if(new RegExp("([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?").test(url)) {
      let b = this.iab.create(url);
      b.show();
    }
  }

}
