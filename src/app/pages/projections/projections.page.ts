import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { ProjectionDetailPage } from '../projection-detail/projection-detail.page';
import { ProjectionsService } from '../../providers/projections/projections.service';
import { TranslateService } from '@ngx-translate/core';
import { Projection } from 'src/app/models/projection/projection';


@Component({
  selector: 'app-projections',
  templateUrl: './projections.page.html',
  styleUrls: ['./projections.page.scss'],
})
export class ProjectionsPage implements OnInit {

  isLoading = false;
  projection: Projection;
  projections = [];

  constructor(
    public modalController: ModalController, 
    private pservice: ProjectionsService,
    public loadingController: LoadingController,
    private translate: TranslateService
    ) { 

    }

  async presentLoading(msg) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: msg
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss();
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss();
  }

  ngOnInit() {
    this.getProjections();
  }

  async doRefresh(event) {
    await this.getProjections();
    event.target.complete();
  }

  async openDetail(id) {
    const modal = await this.modalController.create({
      component: ProjectionDetailPage,
      componentProps: {
        'id': id
      }
    });
    return await modal.present();
  }

  getProjections(){
    this.presentLoading(this.translate.instant('LOADING.projections')); 
    this.pservice.getAllProjections().then((response) => {      
      let data = JSON.parse(response.data);
      let p = data['list'];
      Object.keys(data['list']).forEach(function(key,index) {
          let url = 'http://kumovicci.com/jbcapital/data/upload/'+data['list'][key].imagenId;
          data['list'][key].urlimg = url;
          data['list'][key].createdAt = data['list'][key].createdAt+' UTC';
      });
      this.projections = p;
      console.log(this.projections);
      
      this.dismiss();
    }).catch((error: any) => {
      this.dismiss();
      alert(JSON.stringify(error));
    });
  }

}
