import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProjectionsPage } from './projections.page';
import { TranslateModule } from '@ngx-translate/core';
import { ProjectionDetailPageModule } from '../projection-detail/projection-detail.module';
import { ProjectionDetailPage } from '../projection-detail/projection-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ProjectionsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [ProjectionsPage]
})
export class ProjectionsPageModule {}
