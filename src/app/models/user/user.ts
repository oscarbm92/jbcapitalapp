export interface User {
    "id": string,
    "name": string,
    "emailAddress": string,
    "profilePicURL": string,
    "idFireBase": string
}
