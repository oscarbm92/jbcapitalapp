import { Component } from '@angular/core';

import { Platform, NavController, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LanguageService } from './services/language.service';
import { Facebook } from '@ionic-native/facebook/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from './models/user/user';
import { FCM } from '@ionic-native/fcm/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  public OpenPay: any;
  //declare OpenPay: any;

  public appPages = [
    {
      title: 'HOME.title',
      url: 'app/tabs/newsfeed',
      icon: 'home'
    },
    {
      title: 'PROFILE.title',
      url: '/profile',
      icon: 'person'
    },
    {
      title: 'CONFERENCES.title',
      url: '/conferences',
      icon: 'videocam'
    },
    {
      title: 'SERVICES.title',
      url: '/services',
      icon: 'briefcase'
    },
    {
      title: 'PAYMENT.title',
      url: '/payment',
      icon: 'card'
    },
    {
      title: 'SETTINGS.title',
      url: '/settings',
      icon: 'settings'
    },
    {
      title: 'HELP.title',
      url: '/support',
      icon: 'help-buoy'
    },
    {
      title: 'BUGS.title',
      url: '/bugs',
      icon: 'bug'
    },
    {
      title: 'LOGOUT.title',
      url: '#logout',
      icon: 'log-out'
    }
  ];

  private user : User; 
  

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private languageService: LanguageService,
    private navCtrl: NavController,
    private fb: Facebook,
    private fireAuth: AngularFireAuth,
    public events: Events,
    public fcm: FCM
  ) {

    this.user = {
      id: '0',
      name: 'No user',
      emailAddress: 'No email',
      profilePicURL: 'nopic.jpg',
      idFireBase: '0'
    }

    events.subscribe('user:created', (user) => {      
      this.user = user;      
    });


    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleBlackTranslucent();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 1000);
      this.languageService.setInitialAppLanguage();
      this.fcm.onNotification().subscribe(data => {
        console.log(data);
        if (data.wasTapped) {
          console.log('Received in background');
        } else {
          console.log('Received in foreground');
        }
      });
    });
  }

  navigate(url){
    if(url === '#logout'){
      this.logout();
    }else{
      this.navCtrl.navigateForward(url);
    }
  }

  logout() {
    this.fireAuth.auth.signOut().then(() => {
      this.fb.logout().then(res =>{
        this.navCtrl.navigateRoot('/login');
      }, error =>{
        console.log(error);
      });
    })
  }
}
