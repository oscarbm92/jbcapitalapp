import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class NewsfeedService {

  public headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Basic YWRtaW46YWtpcmVrYXJlbjEwMTcr'
  };

  private baseUrl = 'http://kumovicci.com/jbcapital/api/v1/';

  constructor(public http: HTTP) { }

  getNewsFeed(){
    return this.http.get(this.baseUrl+'Noticia?sortBy=createdAt&asc=false', {}, this.headers);
  }

  postComment(comment){
    return this.http.post(this.baseUrl+'ComentarioProyeccion', {
      "name": comment.name, 
      "comentario": comment.comment,
      "proyeccionId": comment.projectionId,
      "proyeccionName": comment.projectionName,
      "imagenId": comment.imagenId,
      "imagenName": comment.imagenName,
      "accountId": comment.accountId,
      "accountName": comment.accountName,
      "assignedUserId": "1",
      "assignedUserName": "Admin"
    }, this.headers);
  }
}
