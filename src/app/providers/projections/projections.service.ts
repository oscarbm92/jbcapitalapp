import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class ProjectionsService {

  public headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Basic YWRtaW46YWtpcmVrYXJlbjEwMTcr'
  };

  private baseUrl = 'http://kumovicci.com/jbcapital/api/v1/';

  constructor(public http: HTTP) { }

  getAllProjections(){
    return this.http.get(this.baseUrl+'Proyeccion', {}, this.headers);
  }

  getProjection(id){
    return this.http.get(this.baseUrl+'Proyeccion/'+id, {}, this.headers);
  }

  getProjectionComments(id){
    return this.http.get(this.baseUrl+'ComentarioProyeccion?sortBy=createdAt&asc=false&where[0][type]=equals&where[0][field]=proyeccionId&where[0][value]='+id, {}, this.headers);
  }

  postComment(comment){
    return this.http.post(this.baseUrl+'ComentarioProyeccion', {
      "name": comment.name, 
      "comentario": comment.comment,
      "proyeccionId": comment.projectionId,
      "proyeccionName": comment.projectionName,
      "imagenId": comment.imagenId,
      "imagenName": comment.imagenName,
      "accountId": comment.accountId,
      "accountName": comment.accountName,
      "assignedUserId": "1",
      "assignedUserName": "Admin"
    }, this.headers);
  }

  uploadImage(img){
    return this.http.post('http://kumovicci.com/jbcapital/uploadImage.php', {
      'image': img
    }, this.headers);
  }
}
