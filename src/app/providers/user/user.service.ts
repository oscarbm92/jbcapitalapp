import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Basic YWRtaW46YWtpcmVrYXJlbjEwMTcr'
  };

  private baseUrl = 'http://kumovicci.com/jbcapital/api/v1/';

  constructor(public http: HTTP) { }

  newUser(datauser) {

    this.http.setDataSerializer('json');
    /*var date = new Date(datauser.birthday);
    var d = date.getDate()
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    var format_date = '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);*/
    
    return this.http.post(this.baseUrl+'Account', {
      'name': datauser.displayName,
      'emailAddress': datauser.email,
      //'fechaDeNacimiento': format_date,
      'profilePicURL': datauser.photoURL+'?width=512', 
      'idFireBase': datauser.uid
    }, this.headers);
  }

  setUserFirebaseToken(uid, token){
    this.http.setDataSerializer('json');
    return this.http.patch(this.baseUrl+'Account/'+uid, {
      'fbtoken': token
    }, this.headers);
  }

  getUserWithUid(uid){
    return this.http.get(this.baseUrl+'Account?where[0][type]=equals&where[0][field]=idFireBase&where[0][value]='+uid, {}, this.headers);
  }
}
